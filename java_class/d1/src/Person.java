public class Person{
    private String name; // access - data type - modifier
    private int age;

    /*     COMPOSITION --
            having another Object as attribute of your object
            e.g. Pet object inside Person object        */
    private Pet myPet;


/*      ACCESSORS - getting methods
        MUTATORS - setter methods   */

    // arguments with just 2 or nothing, it'll still work.
//    public Person() {
//
//    }

    //    setter
    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }

    // getters
    public int getAge() {
        return age;
    }

    public Pet getMyPet() {
        return myPet;
    }

    public String getName() {
        return name;
    }



//    public Person(String arg1) {
//        this.name = arg1;
//    }
//
//    public Person(int arg1) {
//        this.age = arg1;
//    }

    public Person(String arg1, int arg2) {
        this.name = arg1;
        this.age = arg2;
    } // end of public Person

    public String sayGoodAfternoon() {
        return "Hello. My pet is " + this.myPet.name;
    }

    public String saySomething(String arg1) {
        return this.sayGoodAfternoon() + arg1 + "I'm " + this.name + ". Nice to meet you.";
    }

    public String toString() {
        return "Person: \n"
                + "\tname: " + this.name
                + "\n\tage: " + this.age
                + "\n\t " + this.myPet.toString();
    }
} // end of public class personass person