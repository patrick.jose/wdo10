public class Doctor extends Professional {
    public Doctor() {}
    public Doctor(String name, int age, int yoxp, String powork) {
        super(name, age, yoxp, powork);
    }

    public String treat() {
        return "treating...";
    }

    public String toString() {
        return "Doctor: "
                + super.toString(); // Prints out the super under public Doctor
    }
}
