public class Main {

    public Main() {

    }

    public static void main(String[] args) {

        Pokemon pokemon1 = new Pokemon("Charizard", 100, 50, 45);
        Pokemon pokemon2 = new Pokemon("Blastoise", 100, 50, 45);
        Battle b1 = new Battle(pokemon1, pokemon2);
        Battle.fight(pokemon1, pokemon2);

    }

}

