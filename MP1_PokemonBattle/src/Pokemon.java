public class Pokemon {
    private String name;
    private int hp = 0;
    private int ap = 0;
    private int defense;

    public Pokemon(String name, int hp, int ap, int defense) {
        this.name = name;
        this.hp = hp;
        this.ap = ap;
        this.defense = defense;
    }

    public void setName(String Name) {
        this.name = this.name;
    }

    public String getName() {
        return this.name;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public int getHP() {
        return this.hp;
    }

    public void setAP(int ap) {
        this.ap = ap;
    }

    public int getAP() {
        return this.ap;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getDefense() {
        return this.defense;
    }
}
