public class Battle {
    public Pokemon pokemon1;
    public Pokemon pokemon2;

    public Battle(Pokemon pokemon1, Pokemon pokemon2) {
        this.pokemon1 = pokemon1;
        this.pokemon2 = pokemon2;
    }

    public static void fight(Pokemon pokemon1, Pokemon pokemon2) {
        int i = 0;
        pokemon1.setAP(pokemon1.getAP() - pokemon2.getDefense());
        pokemon2.setAP(pokemon2.getAP() - pokemon1.getDefense());

        do {
            pokemon2.setHP(pokemon2.getHP() - pokemon1.getAP());
            pokemon1.setHP(pokemon1.getHP() - pokemon2.getAP());
            ++i;
        } while(pokemon1.getHP() >= 1 && pokemon2.getHP() >= 1);

        System.out.println(pokemon1.getName() + " will take " + i + " rounds to beat " + pokemon2.getName());
        System.out.println(pokemon2.getName() + " will take " + i + " rounds to beat " + pokemon1.getName());
        if (pokemon1.getHP() == 0 && pokemon2.getHP() == 0) {
            System.out.println("Its a draw!");
        } else if (pokemon1.getHP() < 1) {
            System.out.println(pokemon1.getName() + " has lost the fight");
        } else {
            System.out.println(pokemon2.getName() + " has lost the fight");
        }

    }
}