/*  I N H E R I T A N C E - 'extends' another class. 'IS-A' relationship
    Teacher IS A professional
    Teacher class is the subclass, Professional class is the super class
*/

public class Teacher extends Professional{ // child/subclass extends to parent/superclass
//    private String name;
//    private int age;
//    private int yoxp;
//    private String

    public Teacher() {}
    public Teacher(String name, int age, int yoxp, String powork) {
//        this.name = name;
//        this.age = age;
//        this.yoxp = yoxp;
//        this.powork = powork;
        super(name, age, yoxp, powork);
    }

    public String teach() {
        return "teaching...";
    }

    public String toString() {
        return "Teacher: " + super.toString();
    }
} // end of Public class Teacher