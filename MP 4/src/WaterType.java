public class WaterType extends Pokemon {
    private String weakness = "Grass";
    private String type = "Water";

    public WaterType(String name, double HP, int AP)
    {
        super(name, HP, AP);
    }
    public String getWeakness()
    {
        return weakness;
    }

    public String getType()
    {
        return type;
    }

}