public abstract class Pokemon implements PokeDex {

    private String name;
    private double HP;
    private int AP;

    public Pokemon(){}

    public Pokemon(String name, double HP, int AP)
    {
        this.name = name;
        this.HP = HP;
        this.AP = AP;
    }

    public String getName()
    {
        return this.name;
    }

    public double getHP()
    {
        return this.HP;
    }

    public int getAP()
    {
        return this.AP;
    }


}