public class FireType extends Pokemon {
    private String weakness = "Water";
    private String type = "Fire";

    public FireType(String name, double HP, int AP)
    {
        super(name, HP, AP);
    }
    public String getWeakness()
    {
        return weakness;
    }

    public String getType()
    {
        return type;
    }

}