public class Main {

    public static void main(String[] args) {
        GrassType Bulbasaur = new GrassType("Bulbasaur", 101, 10);
        FireType Charmander = new FireType("Charmander", 101, 10);
        WaterType Squirtle = new WaterType("Squirtle", 100, 10);

        System.out.println(Bulbasaur.toString());
        System.out.println(Charmander.toString());
        System.out.println(Squirtle.toString());

        Battle b1 = new Battle(Bulbasaur, Charmander);

        b1.fight();
    }
}

//public class Main {
//    public static void main(String[] args) {
//
//        //Create an instance each of GrassType, FireType, and Water
//
//        GrassType bulbasaur = new GrassType("Bulbasaur", 101, 10);
//        Firetype charmander = new FireType ("Charmander", 101, 10);
//        WaterType squirtle = new WaterType ("Squirtle", 100, 10);
//
//        // Create instances of Battle class and 2 Pokemons
//
//        Battle b1 = new Battle (bulbasaur, charmander);
//
//        // Use the fight method of the Battle instance to simulate a
//        b1.fight();
//
//
//    }
//}