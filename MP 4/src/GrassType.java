public class GrassType extends Pokemon {
    private String weakness = "Fire";
    private String type = "Grass";

    public GrassType(String name, double HP, int AP)
    {
        super(name, HP, AP);
    }
    public String getWeakness()
    {
        return weakness;
    }

    public String getType()
    {
        return type;
    }

}