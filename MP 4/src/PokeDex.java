public interface PokeDex
{
    public String getType();
    public String getWeakness();
}