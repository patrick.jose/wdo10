public class Battle
{
    private Pokemon p1;
    private Pokemon p2;


    public Battle(Pokemon p1, Pokemon p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }

    public void fight()
    {
//        int trueAtk = p2 hp / dmg p1;
        int p1AP = p1.getAP();
        int p2AP = p2.getAP();

        if(p2.getWeakness() == p1.getType())
        {
            p1AP *= 2;
        }
        if(p1.getWeakness() == p2.getType())
        {
            p2AP *= 2;
        }

        double p1Rounds = p2.getHP() / p1AP;
        double p2Rounds = p1.getHP() / p2AP;

        System.out.println("\n" + p1.getName() + " will deal " + p1AP + " damage per round");
        System.out.println("It will take " + Math.ceil(p1Rounds)+ " rounds for " +p1.getName()+ " to defeat " + p2.getName() + "\n");
        System.out.println("\n" + p2.getName() + " will deal " + p2AP + " damage per round");
        System.out.println("It will take " +Math.ceil(p2Rounds)+ " rounds for " +p2.getName()+ " to defeat " + p1.getName() + "\n");

        if(p1Rounds < p2Rounds)
        {
            System.out.println(p1.getName() + " Wins");
        }
        else if(p2Rounds < p1Rounds)
        {
            System.out.println(p2.getName() + " Wins");
        }
        else if(p1Rounds == p2Rounds)
        {
            System.out.println("The Battle Resulted in a draw");
        }


//        System.out.println("p1 ap: "+p1AP+" and p2 ap: "+p2AP);
    }
}
