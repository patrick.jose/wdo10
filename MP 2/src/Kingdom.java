public class Kingdom {
    public Castle c1;
    public Castle c2;
    public Castle c3;

    public Kingdom(Castle c1, Castle c2, Castle c3) {
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
    }

    public String displayCastleInfo() {
        String var10000 = this.c1.toString();
        return var10000 + this.c2.toString() + this.c3.toString();
    }
}
