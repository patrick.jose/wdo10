public class Castle {
    public String name;
    public String x;
    public String y;

    public Castle(String name, String x, String y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "\n\t" + this.name + "\tx: " + this.x + "\ty: " + this.y;
    }
}
