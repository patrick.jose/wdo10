public class Human extends Fellowship {
    private String weapontype = "Sword";
    private String weaponname;

    public Human(String name, String weaponname) {
        super(name);
        this.weaponname = weaponname;
    }


    public String getWeaponType(){
        return weapontype;
    }

    public String getWeaponName(){
        return weaponname;
    }

    public String toString(){
        return "Human: "  + getName() + ", " + "weaponType: " +getWeaponType() + ", " + "weaponName: " + getWeaponName();

    }

}