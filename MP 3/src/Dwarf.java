public class Dwarf extends Fellowship{
    private String weapontype = "Axe";
    private String weaponname;

    public Dwarf(String name, String weaponname){
        super(name);
        this.weaponname = weaponname;
    }

    public String getWeaponType(){
        return weapontype;
    }

    public String getWeaponName(){
        return weaponname;
    }

    public String toString(){
        return "Dwarf: " +getName() + ", " + "weaponType: " +getWeaponType() + ", " + "weaponName: " + getWeaponName();

    }

}