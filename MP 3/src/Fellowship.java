public class Fellowship {

    private String name1;

    public Fellowship(String name1){
        this.name1 = name1;
    }

    public String getName(){
        return name1;
    }

    public String getWeaponType(){
        return "none";
    }

    public String getWeaponName(){
        return "none";
    }


    public String toString(){
        return "Fellow: " + "name: " + getName() + ", " + "weaponType: " + getWeaponType() + ", " + "weaponName: " +getWeaponName();

    }

}