public class Elf extends Fellowship {
    private String weapontype = "Bow and Arrow";
    private String weaponname;


    public Elf(String name, String weaponname) {
        super(name);
        this.weaponname = weaponname;
    }


    public String getWeaponType() {
        return weapontype;
    }

    public String getWeaponName() {
        return weaponname;
    }


    public String toString() {
        return "Elf: " + getName() + ", " + "weaponType: " + getWeaponType() + ", " + "weaponName: " + getWeaponName();
    }
}