public class Main {
    public static void main (String[] args) {
        // create an instance of each classes

        Fellowship frodo = new Fellowship ("Frodo");
        Elf legolas = new Elf ("Legolas", "Mirkwood Bow");
        Human aragorn = new Human ("Aragorn", "Anduril");
        Dwarf gimli = new Dwarf ("Gimli", "Parashu");
        //call the toString method of each instantiated object

        System.out.println(frodo.toString ());
        System.out.println(legolas.toString ());
        System.out.println(aragorn.toString ());
        System.out.println(gimli.toString ());
    }
}