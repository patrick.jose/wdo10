import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        TeamA teamA = new TeamA();
        ArrayList<Inhuman> team1 = new ArrayList();
        ArrayList<Inhuman> team2 = new ArrayList();
        Scanner scanner = new Scanner(System.in);

        String input;
        String namee;
        AlphaPrimitive alphaPrim;
        BirdPeople birdPeople;
        SuperInhuman superInhuman;
        do {
            System.out.println("Create an Inhuman for Team A\n1: Alpha Primitive\n2: Bird People\n3: Super Inhuman\n4: Exit");
            input = scanner.nextLine();
            if (input.equals("1")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                alphaPrim = new AlphaPrimitive(namee);
                team1.add(alphaPrim);
                System.out.println(alphaPrim.display());
            } else if (input.equals("2")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                birdPeople = new BirdPeople(namee);
                team1.add(birdPeople);
                birdPeople.getHP();
                birdPeople.getAP();
                birdPeople.getDefense();
                System.out.println(birdPeople.display());
            } else if (input.equals("3")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                superInhuman = new SuperInhuman(namee);
                team1.add(superInhuman);
                superInhuman.getHP();
                superInhuman.getAP();
                superInhuman.getDefense();
                System.out.println(superInhuman.display());
            }
        } while(!input.equals("4"));

        teamA.setTeamMember(team1);
        System.out.println(teamA.toString());
        Iterator var7 = team1.iterator();

        while(var7.hasNext()) {
            Inhuman i = (Inhuman)var7.next();
            System.out.println(i);
        }

        do {
            System.out.println("Create an Inhuman for Team B\n1: Alpha Primitive\n2: Bird People\n3: Super Inhuman\n4: Exit");
            input = scanner.nextLine();
            if (input.equals("1")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                alphaPrim = new AlphaPrimitive(namee);
                team2.add(alphaPrim);
                System.out.println(alphaPrim.display());
            } else if (input.equals("2")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                birdPeople = new BirdPeople(namee);
                team2.add(birdPeople);
                System.out.println(birdPeople.display());
            } else if (input.equals("3")) {
                System.out.println("Name your inhuman...");
                namee = scanner.nextLine();
                superInhuman = new SuperInhuman(namee);
                team2.add(superInhuman);
                System.out.println(superInhuman.display());
            }
        } while(!input.equals("4"));

    }
}
