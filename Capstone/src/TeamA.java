import java.util.ArrayList;

public class TeamA {
    private ArrayList<Inhuman> teamMember;

    public TeamA() {
    }

    public void setTeamMember(ArrayList<Inhuman> team) {
        this.teamMember = team;
    }

    public ArrayList<Inhuman> getTeamMember() {
        return this.teamMember;
    }

    public String toString() {
        return "Team A: " + this.getTeamMember();
    }
}
