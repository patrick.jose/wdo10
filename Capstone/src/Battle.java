
public class Battle {
    public TeamA teamA;
    public TeamB teamB;

    public Battle(TeamA teamA, TeamB teamB) {
        this.teamA = teamA;
        this.teamB = teamB;
    }
}
