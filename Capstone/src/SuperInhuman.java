import java.util.Random;

public class SuperInhuman extends Inhuman {
    private String name = "char";
    private int ap = getRandomNumberInRange(100, 200);
    private int hp = getRandomNumberInRange(40, 60);
    private int defense = getRandomNumberInRange(10, 20);

    public SuperInhuman(String name) {
        this.name = name;
        this.setHP(this.hp);
        this.setAP(this.ap);
        this.setDefense(this.defense);
    }

    private static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        } else {
            Random r = new Random();
            return r.nextInt(max - min + 1) + min;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public int getHP() {
        return this.hp;
    }

    public void setAP(int ap) {
        this.ap = ap;
    }

    public int getAP() {
        return this.ap;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getDefense() {
        return this.defense;
    }

    public String display() {
        String var10000 = this.getName();
        return "Super Inhuman: \n\t\tName: " + var10000 + "\n\t\tHP: " + this.hp + "\n\t\tAP: " + this.ap + "\n\t\tDEF: " + this.defense;
    }
}
