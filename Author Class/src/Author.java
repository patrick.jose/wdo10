public class Author {
    public String name;
    public String dob;
    public String pob;
    public String penName;

    public Author(String arg1, String arg2, String arg3) {
        this.name = arg1;
        this.dob = arg2;
        this.pob = arg3;
    }

    public Author(String arg1, String arg2, String arg3, String arg4) {
        this.name = arg1;
        this.dob = arg2;
        this.pob = arg3;
        this.penName = arg4;
    }

    public String toString() {
        return "author: \n\t\tname: " + this.name + "\n\t\tdate of birth: " + this.dob + "\n\t\tplace of birth: " + this.pob + "\n\t\tpen name: " + this.penName;
    }
}

//public class Author {
//    public String name;
//    public String dob;
//    public String pob;
//    public String penName;
//
//
//    public Author(String name, String dob, String pob,);
//        this.name = name;
//        this.dob = dob;
//        this.pob = pob;
//
//    public Author(String name,String dob, String pob, String penName);
//        this.name = name;
//        this.dob = dob;
//        this.pob = pob;
//        this.penName = penName;
//
//    @Override
//    public String toString() {
//        return "Author;"
//                +"\n\tname: " + this.name
//                +"\n\tdate of birth: " + this.dob
//                +"\n\tplace of birth: " + this.pob
//                +"\n\tpenName: " + this.penName;
//    }
//}
//
