public class Book {
    public String title;
    public String author;
    public String genre;
    public String subtitle;
    public Author bookAuthor;

    public Book(String arg1, String arg2, String arg3) {
        this.title = arg1;
        this.author = arg2;
        this.genre = arg3;
    }

    public Book(String arg1, String arg2, String arg3, String arg4) {
        this.title = arg1;
        this.author = arg2;
        this.genre = arg3;
        this.subtitle = arg4;
    }

    public String toString() {
        String var10000 = this.title;
        return "Book: \n\ttitle: " + var10000 + "\n\t " + this.bookAuthor.toString() + "\n\tgenre: " + this.genre + "\n\tsubtitle: " + this.subtitle;
    }
}
