public class Point3D extends Point {
    private int z;

    public Point3D() {
    }

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public Point3D(Point p, int z) {
        super(p.getPointX(), p.getPointY());
        this.z = z;
    }

    public String introduce() {
        return "3D Point";
    }

    public int getZ() {
        return this.z;
    }

    public String toString() {
        int var10000 = this.getPointX();
        return "3D Point: \n\tx: " + var10000 + "\n\ty: " + this.getPointY() + "\n\tz: " + this.z;
    }
}

//public class Point3D extends Point { // subclass
//    private int z;
//
//    public Point3D() {}
//    public Point3D(int x, int y, int z) {
//        super(x, y);
//        this.z = z;
//    }
//
//    public Point3D(Point p, int z) {
//        super(p.getPointX(), p.getPointY());
//        this.z = z;
//    }
//
//    public String introduce() {
//        return "3D Point";
//    }
//
//    public int getZ() {
//        return this.z;
//    }
//
//    public String toString() {
//        return "3D Point: "
//                + "\n\tx: " + this.getPointX()
//                + "\n\ty: " + this.getPointY()
//                +  "\n\tz: " + this.z;
//    }
//} // end of public class point3d
//
