/*
    COMPOSITION - object as attribute of another object
    INHERITANCE - class 'extends 'class
    ENCAPSULATION -  private attributes and methods, use accessors and mutators
    POLYMORPHISM - object can be 2 object types
            (watch out for overload methods)
    ABSTRACTION -
*/
public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Point p1 = new Point(1, 2);
        p1.setPointX(11);
        p1.setPointY(21);
        System.out.println(p1.toString());
        Point3D p3 = new Point3D(p1, 33);
        System.out.println(p1.introduce());
        System.out.println(p3.introduce());
    }
}
